/*
 * temp_sensor.h
 *
 *  Created on: 2016. �pr. 7.
 *      Author: Andras
 */

#ifndef INC_TEMP_SENSOR_H_
#define INC_TEMP_SENSOR_H_

#include "stm32l0xx_hal.h"



//TEMP SENSOR
#define I2C_TIMING 0x00300208 //400kHz, 300ns Rise Time, 300ns Fall Time, Analog filter enable
#define I2C_ADDRESS 0b10011000

//TEMP SENSOR REGISTER ADDRESS
#define LOCAL_TEMP_H 0x00
#define LOCAL_TEMP_L 0x15
#define STATUS 0x02
#define CONV_RATE_R 0x04
#define CONV_RATE_W 0x0A
#define REMOTE_TEMP_H 0x01
#define REMOTE_TEMP_L 0x10
#define CONF_REG1_W 0x09
#define CONF_REG2_W 0x1A
#define BETA_RANGE 0x25


extern I2C_HandleTypeDef hi2c1;

//I2C_rx_buffer[0]=Local_H
//I2C_rx_buffer[1]=Local_L
//I2C_rx_buffer[2]=Remote_H
//I2C_rx_buffer[3]=Remote_L
extern uint8_t I2C_rx_buffer[4];//raw temperature values
extern uint8_t I2C_tx_buffer[4];
extern uint8_t remote_detected; // '0' if the remote transistor is NOT detected

extern const uint8_t temp_decimal_fraction_lookup[];
extern uint16_t temp_local; // rounded "decimal", integer part: 15...8 bits, fractional part: 7...0 bits
extern uint16_t temp_remote; // integer part: 15...8 bits, fractional part: 7...0 bits


void MX_I2C1_Init(void);
void Temp_Sensor_Init(void);
void Detect_Remote_Temp_Sensor(void);
void Temp_Sensor_Read(void);
void Convert_Temperature_To_Decimal(uint16_t *temp_var, uint8_t* I2C_buffer, uint8_t buffer_start_index);

#endif /* INC_TEMP_SENSOR_H_ */
