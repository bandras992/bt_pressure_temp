/*
 * gpio.h
 *
 *  Created on: 2016. �pr. 10.
 *      Author: Andras
 */

#ifndef INC_GPIO_H_
#define INC_GPIO_H_

#include "stm32l0xx_hal.h"


void MX_GPIO_Init(void);
void Turn_Off(void);

#endif /* INC_GPIO_H_ */
