/*
 * adc.h
 *
 *  Created on: 2016. �pr. 7.
 *      Author: Andras
 */

#ifndef INC_ADC_H_
#define INC_ADC_H_


#include "stm32l0xx_hal.h"

//ADC
// Vchannel=(3*vrefint_cal*ADC_data) / (vrefint_data*FULL_SCALE)

#define FULL_SCALE 4095
#define VREFINT_CAL_ADDR 0x1FF80078 //VRERINT_CAL MSB address (data stored in little-endian format)
#define PRESSURE_OFFSET_ADDR 0x08080000 // EEPROM start address

extern ADC_HandleTypeDef hadc;
extern ADC_ChannelConfTypeDef sConfig;

extern volatile uint8_t ADC_IT_flag;

extern uint16_t vrefint_cal;//VREFINT calibration value
extern uint16_t vrefint_data; //VREFINT measured value

//battery level
#define BATTERY_VOLTAGE_MAX 1300	//1300mV
#define BATTERY_VOLTAGE_MIN 900		//900mV

extern uint8_t battery_level; // 0-100%

//pressure
extern uint32_t pressure;
extern int32_t pressure_DC_offset;

#define ATMOSPHERIC_PRESSURE 100 //(kPa)

void MX_ADC_Init(void);
uint16_t ADC_Avg_Value(uint32_t channel); // calculate average from 10 ADC measurements
uint32_t ADC_Get_Voltage (uint32_t channel); //Vchannel(mV)=(3000*vrefint_cal*ADC_data*) / (vrefint_data*FULL_SCALE)
uint32_t Get_Battery_Voltage(uint32_t channel); //10k series resistance compensated
void Get_Battery_Level(uint32_t channel);

//int32_t Get_Pressure_DC_Offset(uint32_t channel);
uint32_t Get_Pressure(uint32_t channel);//return the pressure value in kPa
void Remove_Pressure_Offset(void);
void Write_Pressure_Offset_To_EEPROM(void);
void Read__Pressure_Offset_From_EEPROM(void);


#endif /* INC_ADC_H_ */
