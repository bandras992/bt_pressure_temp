/*
 * circular_buffer.h
 *
 *  Created on: 2016. m�rc. 3.
 *      Author: Andras
 */

#ifndef SRC_CIRCULAR_BUFFER_H_
#define SRC_CIRCULAR_BUFFER_H_

#include "stm32l0xx_hal.h"


//FIFO circular buffer


#define C_BUFFER_LENGHT 100

typedef struct {
	uint8_t buffer[C_BUFFER_LENGHT];
	uint8_t start_index; // index of the first element
	uint8_t end_index; // index of the next place
	uint8_t number_of_elements;
} circular_buffer;

extern circular_buffer c_buffer;



void Add_Array_To_C_Buffer(uint8_t *array, uint8_t array_size, circular_buffer *c_buff);
void Add_Element_To_C_Buffer(uint8_t element, circular_buffer *c_buff);
void Delete_Element_From_C_Buffer(circular_buffer *c_buff);
void Delete_Elements_From_C_Buffer(circular_buffer *c_buff, uint8_t number_of_elements);
void Transfer_Element_From_C_Buffer_To_Variable(circular_buffer *c_buff, uint8_t* var);
void Transfer_Elements_From_C_Buffer_To_Array(circular_buffer *c_buff, uint8_t* array, uint8_t number_of_elements );


#endif /* SRC_CIRCULAR_BUFFER_H_ */
