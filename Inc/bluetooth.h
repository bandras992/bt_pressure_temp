/*
 * bluetooth.h
 *
 *  Created on: 2016. �pr. 9.
 *      Author: Andras
 */

#ifndef INC_BLUETOOTH_H_
#define INC_BLUETOOTH_H_

#include "LED.h"
#include "temp_sensor.h"
#include "adc.h"
#include "circular_buffer.h"
#include "gpio.h"

//UART
UART_HandleTypeDef huart1;
volatile uint8_t uart_IT_flag;

//Bluetooth initialization
typedef struct{
	uint8_t size;
	const char *command;
}BT_Command;

extern const uint8_t BT_init_array_size;
extern const BT_Command BT_init_array[];
extern const BT_Command BT_command_array[];

//hex<->binary conversion
extern const uint8_t hex_lookup[];
extern const uint8_t index_order[];



//write value to characteristic
#define UUID_SIZE 32 // 128 bit private services (version 4) UUIDs
extern const uint8_t private_service_UUID[UUID_SIZE]; //0x2bdb51c1698742bc9af1221990841f65
extern const uint8_t local_temp_service_UUID[UUID_SIZE]; //0xf7ec04e2ec8a4c8b86889a3c4a38c242
extern const uint8_t remote_temp_service_UUID[UUID_SIZE]; //0x496c882ed6d34e9fa52922eab67ed0bf
extern const uint8_t pressure_service_UUID[UUID_SIZE]; //0x10fb6ed655104e69980b453af4ead084
extern const uint8_t LED_service_UUID[UUID_SIZE]; //0x7f28fd0a3618402689829615d296fe07
extern const uint8_t turn_off_service_UUID[UUID_SIZE]; //0x3bf14ce8f0bb48b79c3ce1accd21eafa
extern const uint8_t battery_level_UUID[4];

extern uint8_t hex_array[8];
extern uint8_t LED_rx_buff[8];

//process received bluetooth notification
extern uint8_t AOK_received_flag;
extern uint8_t CMD_received_flag;
extern uint8_t uart_state;

extern uint8_t BT_ready_flag;
extern uint8_t BT_init_state;

extern const uint8_t handle_LED[4];
extern const uint8_t handle_turn_off[4];

typedef struct{
	uint8_t size;
	const char *message;
}BT_Notification;

extern const BT_Notification notification[];



//Bluetooth initialization
void MX_USART1_UART_Init(void);
void BT_Init(void);
void  BT_Init_SM(void);

void RXNE_IT_Enable(UART_HandleTypeDef *huart);

//hex<->binary conversion
void Binary_To_HEX_Array(uint32_t binary, uint8_t *hex_a, uint8_t hex_char_size);
uint32_t HEX_Array_To_Binary(uint8_t *hex_a, uint8_t size);

//write value to characteristic
void Write_To_Characteristic(uint8_t *UUID, uint8_t UUID_size, uint8_t *value, uint8_t value_size);
void Write_Pressure_To_Characteristic(void);
void Write_Temp_To_Characteristic(void);
void Write_Temp_To_Characteristic(void);
void Write_Battery_Level_To_Characteristic(void);
void Write_LED_Color_To_Characteristic(void);


//void Read_LED_Color_From_Characteristic(void); //ez majd nem kell

//process received bluetooth notification
uint8_t array_compare(uint8_t *array1, uint8_t *array2, uint8_t size);
void UART_State_Machine(circular_buffer *c_buff);

void Add_UART_RDR_To_C_Buffer(UART_HandleTypeDef *huart,circular_buffer *c_buff);


#endif /* INC_BLUETOOTH_H_ */
