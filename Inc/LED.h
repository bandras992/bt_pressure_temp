/*
 * LED.h
 *
 *  Created on: 2016. �pr. 7.
 *      Author: Andras
 */

#ifndef INC_LED_H_
#define INC_LED_H_

#include "stm32l0xx_hal.h"


//color[0] : Red value (  0-249 )
//color[1] : Green value (  0-249 )
//color[2] : Blue value (  0-249 )
extern uint8_t color[3];
TIM_HandleTypeDef htim2;

void MX_TIM2_Init(void);
void Set_LED_Color(uint8_t *color);

#endif /* INC_LED_H_ */
