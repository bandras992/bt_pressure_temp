/*
 * circular_buffer.c
 *
 *  Created on: 2016. m�rc. 3.
 *      Author: Andras
 */

#include "circular_buffer.h"


void Add_Array_To_C_Buffer(uint8_t *array, uint8_t array_size, circular_buffer *c_buff){
	uint8_t i;


	for(i=0; i < array_size; i++){
		Add_Element_To_C_Buffer(array[i], c_buff);
	}
}


void Add_Element_To_C_Buffer(uint8_t element, circular_buffer *c_buff){

	if( C_BUFFER_LENGHT <  (c_buff->number_of_elements + 1) ){ //to prevent overwrite
	}

	else{
		c_buff->buffer[c_buff->end_index] = element;

		(c_buff->end_index)++;

		if(c_buff->end_index >= C_BUFFER_LENGHT ){
			c_buff->end_index=0;
		}

		c_buff->number_of_elements++;

	}
}



void Delete_Element_From_C_Buffer(circular_buffer *c_buff){

	if(c_buff->number_of_elements != 0){

		c_buff->number_of_elements--;
		c_buff->start_index++;

		if((c_buff->start_index) >= C_BUFFER_LENGHT){

			c_buff->start_index = 0;
		}
	}
}


void Delete_Elements_From_C_Buffer(circular_buffer *c_buff, uint8_t number_of_elements){

	uint8_t i;

	for(i=0;i<number_of_elements;i++){

		Delete_Element_From_C_Buffer(c_buff);
	}
}


void Transfer_Element_From_C_Buffer_To_Variable(circular_buffer *c_buff, uint8_t* var){

	if(c_buff->number_of_elements != 0){

		var = &c_buff->buffer[c_buff->end_index];
		Delete_Element_From_C_Buffer(c_buff);
	}
}


void Transfer_Elements_From_C_Buffer_To_Array(circular_buffer *c_buff, uint8_t* array, uint8_t number_of_elements ){

	uint8_t i;

	for(i=0;i<number_of_elements;i++){

		Transfer_Element_From_C_Buffer_To_Variable(c_buff, &array[i]);
	}
}




