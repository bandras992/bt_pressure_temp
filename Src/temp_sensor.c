/*
 * temp_sensor.c
 *
 *  Created on: 2016. �pr. 7.
 *      Author: Andras
 */

#include "temp_sensor.h"

I2C_HandleTypeDef hi2c1;

//I2C_rx_buffer[0]=Local_H
//I2C_rx_buffer[1]=Local_L
//I2C_rx_buffer[2]=Remote_H
//I2C_rx_buffer[3]=Remote_L
uint8_t I2C_rx_buffer[4];//raw temperature values
uint8_t I2C_tx_buffer[4];
uint8_t remote_detected=0; // '0' if the remote transistor is NOT detected

const uint8_t temp_decimal_fraction_lookup[]={0, 1, 1, 2, 3, 3, 4, 4, 5, 6, 6, 7, 8, 8, 9, 9};
uint16_t temp_local; // rounded "decimal", integer part: 15...8 bits, fractional part: 7...0 bits
uint16_t temp_remote; // integer part: 15...8 bits, fractional part: 7...0 bits


/* I2C1 init function */
void MX_I2C1_Init(void)
{

  hi2c1.Instance = I2C1;
  hi2c1.Init.Timing = I2C_TIMING;;
  hi2c1.Init.OwnAddress1 = 0;
  hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLED;
  hi2c1.Init.OwnAddress2 = 0;
  hi2c1.Init.OwnAddress2Masks = I2C_OA2_NOMASK;
  hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLED;
  hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLED;

  HAL_I2C_Init(&hi2c1);

  /* Enable the Analog I2C Filter */
  HAL_I2CEx_ConfigAnalogFilter(&hi2c1,I2C_ANALOGFILTER_ENABLE);
}



void Temp_Sensor_Init(void)
{

	//Set conversion rate
	I2C_tx_buffer[0]=(uint8_t) CONV_RATE_W;
	I2C_tx_buffer[1]=0x07; // 8 conversion/sec

	HAL_I2C_Master_Transmit_IT(&hi2c1, (uint16_t) I2C_ADDRESS, (uint8_t*) I2C_tx_buffer, 2);
	while (HAL_I2C_GetState(&hi2c1) != HAL_I2C_STATE_READY); //waiting for the end of current transfer before starting a new one

	I2C_tx_buffer[0]=(uint8_t) CONF_REG1_W;
	I2C_tx_buffer[1]=0b10000000;//ALERT masked, temperature range: 0 to +127
	HAL_I2C_Master_Transmit_IT(&hi2c1, (uint16_t) I2C_ADDRESS, (uint8_t*) I2C_tx_buffer, 2);
	while (HAL_I2C_GetState(&hi2c1) != HAL_I2C_STATE_READY); //waiting for the end of current transfer before starting a new one
	{
	}

	I2C_tx_buffer[0]=(uint8_t) BETA_RANGE;
	I2C_tx_buffer[1]= 0b00000111;//beta correction disabled, N-factor: 1.008   //0b00001111; //Beta automatically detected, diode connected  //0b00000111;//beta correction disabled, N-factor: 1.008
	HAL_I2C_Master_Transmit_IT(&hi2c1, (uint16_t) I2C_ADDRESS, (uint8_t*) I2C_tx_buffer, 2);
	while (HAL_I2C_GetState(&hi2c1) != HAL_I2C_STATE_READY); //waiting for the end of current transfer before starting a new one


	HAL_Delay(200); //waiting for the first conversion and STATUS register update

	Detect_Remote_Temp_Sensor();


}



void Detect_Remote_Temp_Sensor(void){

	//Read and check temp. sensor STATUS register
	I2C_tx_buffer[0]=(uint8_t)STATUS;

	HAL_I2C_Master_Transmit_IT(&hi2c1, (uint16_t) I2C_ADDRESS, (uint8_t*) I2C_tx_buffer, 1);
	while (HAL_I2C_GetState(&hi2c1) != HAL_I2C_STATE_READY); //waiting for the end of current transfer before starting a new one


	HAL_I2C_Master_Receive_IT(&hi2c1, (uint16_t) I2C_ADDRESS, (uint8_t*) I2C_rx_buffer, 1);
	while (HAL_I2C_GetState(&hi2c1) != HAL_I2C_STATE_READY); //waiting for the end of current transfer


	I2C_rx_buffer[0] &= 0b00000100; //mask OPEN bit
	remote_detected = !I2C_rx_buffer[0];

}



void Temp_Sensor_Read(void)
{

	//TEMP SENSOR REGISTER ADDRESS
	I2C_tx_buffer[0]=(uint8_t)LOCAL_TEMP_H;
	I2C_tx_buffer[1]=(uint8_t)LOCAL_TEMP_L;
	I2C_tx_buffer[2]=(uint8_t)REMOTE_TEMP_H;
	I2C_tx_buffer[3]=(uint8_t)REMOTE_TEMP_L;

	uint8_t i;
	uint8_t x= (remote_detected) ? 4 : 2;
	//uint8_t fraction;


	//Read Local Temperature Registers and Read Local Remote Registers if remote transistor is connected
	for(i=0;i<x;i++)
	{
		HAL_I2C_Master_Transmit_IT(&hi2c1, (uint16_t) I2C_ADDRESS, &I2C_tx_buffer[i], 1);
		while (HAL_I2C_GetState(&hi2c1) != HAL_I2C_STATE_READY); //waiting for the end of current transfer before starting a new one


		HAL_I2C_Master_Receive_IT(&hi2c1, (uint16_t) I2C_ADDRESS, &I2C_rx_buffer[i], 1);
		while (HAL_I2C_GetState(&hi2c1) != HAL_I2C_STATE_READY); //waiting for the end of current transfer

	}

	//I2C_rx_buffer[0]=Local_H
	//I2C_rx_buffer[1]=Local_L
	//I2C_rx_buffer[2]=Remote_H
	//I2C_rx_buffer[3]=Remote_L



	// convert temperature to a "decimal" uint16_t, integer part: 15...8 bits, fractional part: 7...0 bits

	//local

	Convert_Temperature_To_Decimal( &temp_local, I2C_rx_buffer, 0);

	//remote
	if(remote_detected){

		Convert_Temperature_To_Decimal( &temp_remote, I2C_rx_buffer, 2);

	}
}

void Convert_Temperature_To_Decimal(uint16_t *temp_var, uint8_t* I2C_buffer, uint8_t buffer_start_index){

	uint8_t fraction;

	*temp_var = I2C_rx_buffer[buffer_start_index];
	*temp_var = *temp_var << 8;

	// rounding fractional
	fraction = I2C_rx_buffer[buffer_start_index+1];
	fraction = fraction >> 4;
	fraction = temp_decimal_fraction_lookup[fraction];
	*temp_var |= (uint16_t) fraction;

}


