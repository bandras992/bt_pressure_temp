#include "bluetooth.h"

circular_buffer c_buffer;

//UART
UART_HandleTypeDef huart1;




const uint8_t BT_init_array_size = 12;
const BT_Command BT_init_array[]={
		//{3,"+\r\n"}, //enable echo
		{6,"SF,1\r\n"}, //factory reset
		{13,"SR,20060000\r\n"}, //set rn4020 as peripheral, automatically start advertisement, MLDP enabled ///20060000-el j�
		{15,"SN,P_T_sensor\r\n"},//set device name
		//{6,"SP,7\r\n"},//transmit power to 7.5 dBm
		{13,"SS,40000001\r\n"}, //Battery service as server role + enable private service support
		{4,"PZ\r\n"}, //clear the current private service and characteristics
		{37,"PS,2BDB51C1698742BC9AF1221990841F65\r\n"}, //set private service UUID
		{43,"PC,F7EC04E2EC8A4C8B86889A3C4A38C242,12,02\r\n" },//local_temp	// Add private characteristic 0xf7ec04e2ec8a4c8b86889a3c4a38c242 to
																		// current private service. The property of this characteristic is 0x22
																		// (readable and could notify) and has a maximum data size of 2 bytes
		{43,"PC,496C882ED6d34E9FA52922EAB67ED0BF,12,02\r\n"},//remote_temp
		{43,"PC,10FB6ED655104E69980B453AF4EAD084,1A,02\r\n"},//pressure (readable+writable to eliminate offset and could notify)
		{43,"PC,7F28FD0A3618402689829615D296FE07,0A,03\r\n"},//LED (readable+writable)
		{43,"PC,3BF14CE8F0BB48B79C3CE1ACCD21EAFA,08,01\r\n"},//turn_off (writable)
		{5,"R,1\r\n"}, //reboot
};


const BT_Command BT_command_array[]={
		{4,"SUW,"},// "SUW"command writes the contents of the characteristic in the server service to a local device by addressing its UUID.
		{4,"SUR,"},// "SUR" command reads the value of the characteristic in the server service on a local device by addressing its UUID.
		{1,","},
		{2,"\r\n"},
		{2,"LS"}
};
// EXAMPLE:
// SUW,f7ec04e2ec8a4c8b86889a3c4a38c242,0734 (Set the local value of the characteristic local_temp_service to be 0x34.0x07 �C = 50.7 �C )
// BT_command_array[0].command + local_temp_service_UUID + BT_command_array[2].command + "0734"+\r\n
// size = BT_command_array[0].size + UUID_SIZE + BT_command_array[2].size + 4 + BT_command_array[3].size


//hex<->binary conversion
const uint8_t hex_lookup[]="0123456789ABCDEF";
const uint8_t index_order[]={0,1,2,3,4,5,6,7}; //index_order[]={1,0,3,2,5,4,7,6}; // Based on RN4020 Bluetooth� Low Energy Module User�s Guide

//write value to characteristic
#define UUID_SIZE 32 // 128 bit private services (version 4) UUIDs
const uint8_t private_service_UUID[UUID_SIZE]="2BDB51C1698742BC9AF1221990841F65"; //0x2bdb51c1698742bc9af1221990841f65
const uint8_t local_temp_service_UUID[UUID_SIZE]="F7EC04E2EC8A4C8B86889A3C4A38C242"; //0xf7ec04e2ec8a4c8b86889a3c4a38c242
const uint8_t remote_temp_service_UUID[UUID_SIZE]="496C882ED6d34E9FA52922EAB67ED0BF"; //0x496c882ed6d34e9fa52922eab67ed0bf
const uint8_t pressure_service_UUID[UUID_SIZE]="10FB6ED655104E69980B453AF4EAD084"; //0x10fb6ed655104e69980b453af4ead084
const uint8_t LED_service_UUID[UUID_SIZE]="7F28FD0A3618402689829615D296FE07"; //0x7f28fd0a3618402689829615d296fe07
const uint8_t turn_off_service_UUID[UUID_SIZE]="3BF14CE8F0BB48B79C3CE1ACCD21EAFA"; //0x3bf14ce8f0bb48b79c3ce1accd21eafa
const uint8_t battery_level_UUID[]="2A19";

uint8_t hex_array[8];
uint8_t LED_rx_buff[8];


uint8_t AOK_received_flag=0;
uint8_t CMD_received_flag=0;
uint8_t uart_state=0;

const uint8_t handle_LED[4]="0018";
const uint8_t handle_turn_off[4]="001A";
const uint8_t handle_pressure[4]="0015";
const BT_Notification notification[]={
		{3, "AOK"},
		{3, "WV,"},//WV,<hex16 Handle>,<data>
		{3, "CMD"},
};




/* USART1 init function */
void MX_USART1_UART_Init(void)
{

  huart1.Instance = USART1;
  huart1.Init.BaudRate = 115200;
  huart1.Init.WordLength = UART_WORDLENGTH_8B;
  huart1.Init.StopBits = UART_STOPBITS_1;
  huart1.Init.Parity = UART_PARITY_NONE;
  huart1.Init.Mode = UART_MODE_TX_RX;
  huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart1.Init.OverSampling = UART_OVERSAMPLING_16;
  huart1.Init.OneBitSampling = UART_ONEBIT_SAMPLING_DISABLED;
  huart1.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_RXOVERRUNDISABLE_INIT;
  huart1.AdvancedInit.OverrunDisable =  UART_ADVFEATURE_OVERRUN_DISABLE;
  HAL_UART_Init(&huart1);

}


void BT_Init(void)
{
	uint8_t i;


	/* PA7(WAKE_HW) PA12(CMD/MLDP) PA15(WAKE_SW) */
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_15, GPIO_PIN_SET); // WAKE_SW pin to high
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_12, GPIO_PIN_SET); // CMD/MLDP
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_7, GPIO_PIN_SET);

	HAL_Delay(400);

	for(i=0; i < BT_init_array_size ; i++){
		HAL_UART_Transmit( &huart1, &BT_init_array[i].command[0], BT_init_array[i].size, 1000  );


		/* marad a sima delay
		 * while(AOK_received_flag);
		 * AOK_received_flag = 0;
		 */

		HAL_Delay(200);
	}

	HAL_Delay(2000);
}

void  BT_Init_SM(){

	if( BT_init_state == 0){
		/* PA7(WAKE_HW) PA12(CMD/MLDP) PA15(WAKE_SW) */
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_15, GPIO_PIN_SET); // WAKE_SW pin to high
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_12, GPIO_PIN_SET); // CMD/MLDP
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_7, GPIO_PIN_SET);

		BT_init_state=1;
	}

	else if(BT_init_state == 1){//waiting for CMD message

		if(CMD_received_flag){
			HAL_UART_Transmit( &huart1, &BT_init_array[0].command[0], BT_init_array[0].size, 1000  );//send the first command
			BT_init_state=2;
			CMD_received_flag=0;
		}
	}

	else if(BT_init_state < BT_init_array_size+1 ){//send commands


		if(AOK_received_flag){// waiting for AOK, before send the next command
			HAL_UART_Transmit( &huart1, &BT_init_array[BT_init_state-1].command[0], BT_init_array[BT_init_state-1].size, 1000  );
			BT_init_state++;
			AOK_received_flag=0;
		}
	}

	else if(BT_init_state == BT_init_array_size+1){// CMD indicates the end of the reboot sequence
		if(CMD_received_flag){//CMD_received_flag

			BT_ready_flag=1;
			CMD_received_flag=0;

			Set_LED_Color(color); //turn greed LED on
			Write_LED_Color_To_Characteristic();
		}

	}

}




void RXNE_IT_Enable(UART_HandleTypeDef *huart){

	/* Enable the UART Data Register not empty Interrupt */
	huart -> Instance -> CR1 |= 0x00000020; //overrun error has been disabled previously in MX_USART1_UART_Init();

}


void Binary_To_HEX_Array(uint32_t binary, uint8_t *hex_a, uint8_t hex_char_size){

	uint8_t i;

	/*
	// Based on RN4020 Bluetooth� Low Energy Module User�s Guide

	 for(i=0;i<hex_char_size;i++)
		hex_a[index_order[i]]=hex_lookup[(binary >> (i << 2)) & 0x0000000F];
	 */

	for(i=0;i<hex_char_size;i++)
		hex_a[index_order[hex_char_size-1-i]]=hex_lookup[(binary >> (i << 2)) & 0x0000000F];

}



uint32_t HEX_Array_To_Binary(uint8_t *hex_a, uint8_t size){

	uint32_t binary = 0;
	uint8_t i;


	for(i=0;i<size;i++){

		binary = binary << 4;

		if(hex_a[i] < 58) // ASCII "0" ... "9"
			binary |= hex_a[i]-48;

		else // ASCII "A" ... "F"
			binary |= hex_a[i]-55;

	}

	return binary;
}



void Write_To_Characteristic(uint8_t *UUID, uint8_t UUID_size, uint8_t *value, uint8_t value_size){
	HAL_UART_Transmit(&huart1,&BT_command_array[0].command[0] ,BT_command_array[0].size ,1000 );//SUW,
	HAL_UART_Transmit(&huart1,UUID ,UUID_size ,1000 );//send private characteristic UUID
	HAL_UART_Transmit(&huart1,&BT_command_array[2].command[0] ,BT_command_array[2].size ,1000 );//  " , "
	HAL_UART_Transmit(&huart1,value,value_size,1000 );// value
	HAL_UART_Transmit(&huart1,&BT_command_array[3].command[0] ,BT_command_array[3].size ,1000 ); // " /r/n "
}



void Write_Pressure_To_Characteristic(void){

	Binary_To_HEX_Array(pressure, hex_array,4);

	Write_To_Characteristic(pressure_service_UUID, UUID_SIZE, hex_array, 4);


}



void Write_Temp_To_Characteristic(void){

	Binary_To_HEX_Array(temp_local, hex_array,4);

	Write_To_Characteristic(local_temp_service_UUID, UUID_SIZE, hex_array, 4);

	if(remote_detected){
		Binary_To_HEX_Array(temp_remote, hex_array,4);

		Write_To_Characteristic(remote_temp_service_UUID, UUID_SIZE, hex_array, 4);
	}

}

void Write_Battery_Level_To_Characteristic(void){

	Binary_To_HEX_Array(battery_level, hex_array,2);

	Write_To_Characteristic(battery_level_UUID, 4, hex_array, 2);
}



void Write_LED_Color_To_Characteristic(void){

	uint8_t i;

	for(i=0;i<3;i++){
		Binary_To_HEX_Array( color[i], &hex_array[(i << 1)] ,2);

	}

	Write_To_Characteristic(LED_service_UUID ,UUID_SIZE, hex_array, 6);
}



uint8_t array_compare(uint8_t *array1, uint8_t *array2, uint8_t size){
	int i;
	int result = 1;


	for(i=0; i<size; i++){
		if(array1[i] != array2[i])
			result = 0;

	}
	return result;
}

void UART_State_Machine(circular_buffer *c_buff){


	switch(uart_state){

		case 0 : //waiting for 'A' or 'W'

			if(c_buff->number_of_elements != 0){

				if(c_buff->buffer[c_buff->start_index] == 'A' || c_buff->buffer[c_buff->start_index] == 'W' || c_buff->buffer[c_buff->start_index] == 'C'){

					uart_state = 1;
				}

				else{
					Delete_Element_From_C_Buffer(c_buff);
				}
			}

		break;


		case 1 :

			if(c_buff->number_of_elements >= 3){ // first 3 bytes of uart notification received

				if(array_compare(notification[0].message, &c_buff->buffer[c_buff->start_index], notification[0].size)){ // AOK received

					AOK_received_flag++;
					Delete_Elements_From_C_Buffer(c_buff,3);
					uart_state = 0;
				}

				else if(array_compare(notification[2].message, &c_buff->buffer[c_buff->start_index], notification[2].size)){ // CMD received

					CMD_received_flag++;
					Delete_Elements_From_C_Buffer(c_buff,3);
					uart_state = 0;
				}


				else if(array_compare(notification[1].message, &c_buff->buffer[c_buff->start_index], notification[1].size)){ //"WV," received

					uart_state = 2;
				}

				else{ // smthg other received
					Delete_Element_From_C_Buffer(c_buff);
					uart_state = 0;
				}
			}

		break;


		case 2 : //WV, received , waiting for handle and data

			//WV,<hex16 Handle>,<data>\r ; sum:  15 - LED, 11-turn_off, 12- pressure
			if(c_buff->number_of_elements >= 15){

				if(array_compare(handle_LED, &c_buff->buffer[c_buff->start_index+3], 4)){
					//value has been written to LED characteristic

					// data: c_buff->buffer[c_buff->start_index+x] ,x=[8..13]

					color[0] = (uint8_t) HEX_Array_To_Binary( &c_buff->buffer[c_buff->start_index+8], 2);
					color[1] = (uint8_t) HEX_Array_To_Binary( &c_buff->buffer[c_buff->start_index+10], 2);
					color[2] = (uint8_t) HEX_Array_To_Binary( &c_buff->buffer[c_buff->start_index+12], 2);

					Set_LED_Color(color);

					Delete_Elements_From_C_Buffer(c_buff,15);
					uart_state = 0;
				}

				else if(array_compare(handle_turn_off, &c_buff->buffer[c_buff->start_index+3], 4)){
					//value has been written to turn_off characteristic

					// data: c_buff->buffer[c_buff->start_index+x] ,x=[8..9]

					if((uint8_t)'O' == (uint8_t) HEX_Array_To_Binary( &c_buff->buffer[c_buff->start_index+8], 2))
						Turn_Off();

					Delete_Elements_From_C_Buffer(c_buff,11);
					uart_state = 0;
				}

				else if(array_compare(handle_pressure, &c_buff->buffer[c_buff->start_index+3], 4)){
					//value has been written to pressure characteristic

					// data: c_buff->buffer[c_buff->start_index+x] ,x=[8..11]

					//eliminate offset

					pressure = Get_Pressure(ADC_CHANNEL_5);
					pressure_DC_offset= pressure - (int32_t) HEX_Array_To_Binary( &c_buff->buffer[c_buff->start_index+8], 4);

					Write_Pressure_Offset_To_EEPROM();

					Remove_Pressure_Offset();

					uart_state = 0;
				}


				else{// smthg other received
					Delete_Element_From_C_Buffer(c_buff);
					uart_state = 0; //jump to the start
				}
			}

		break;


		default :

			uart_state = 0;

		break;

	}
}


void Add_UART_RDR_To_C_Buffer(UART_HandleTypeDef *huart,circular_buffer *c_buff){
	Add_Element_To_C_Buffer( huart -> Instance -> RDR, c_buff);
}


