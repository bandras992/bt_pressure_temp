/*
 * adc.c
 *
 *  Created on: 2016. �pr. 7.
 *      Author: Andras
 */

#include "adc.h"

ADC_HandleTypeDef hadc;
ADC_ChannelConfTypeDef sConfig;

volatile uint8_t ADC_IT_flag=0;

uint16_t vrefint_cal;  //VREFINT calibration value
uint16_t vrefint_data; //VREFINT measured value

uint8_t battery_level;

uint32_t pressure;
int32_t pressure_DC_offset = 0;


/* ADC init function */
void MX_ADC_Init(void)
{

	vrefint_cal = *((uint16_t*)VREFINT_CAL_ADDR);

     /**Configure the global features of the ADC (Clock, Resolution, Data Alignment and number of conversion)
    */
	hadc.Instance = ADC1;
	hadc.Init.OversamplingMode = DISABLE;
	hadc.Init.ClockPrescaler = ADC_CLOCKPRESCALER_PCLK_DIV2; //4Mhz
	hadc.Init.Resolution = ADC_RESOLUTION12b;
	hadc.Init.SamplingTime = ADC_SAMPLETIME_28CYCLES_5;
	hadc.Init.ScanConvMode = ADC_SCAN_DIRECTION_FORWARD;
	hadc.Init.DataAlign = ADC_DATAALIGN_RIGHT;
	hadc.Init.ContinuousConvMode = DISABLE;
	hadc.Init.DiscontinuousConvMode = DISABLE;
	hadc.Init.ExternalTrigConvEdge =ADC_EXTERNALTRIG_EDGE_NONE;// ADC_SOFTWARE_START;
	hadc.Init.DMAContinuousRequests = DISABLE;
	hadc.Init.EOCSelection = ADC_EOC_SINGLE_CONV;
	hadc.Init.Overrun = ADC_OVR_DATA_OVERWRITTEN;
	hadc.Init.LowPowerAutoWait = DISABLE;
	hadc.Init.LowPowerFrequencyMode = DISABLE;
	hadc.Init.LowPowerAutoPowerOff = ENABLE;
	HAL_ADC_Init(&hadc);

	while(HAL_ADCEx_Calibration_Start(&hadc, ADC_SINGLE_ENDED) != HAL_OK );//ADC calibration


    /**Configure for the selected ADC regular channel to be converted. */

	sConfig.Channel = ADC_CHANNEL_3;
	HAL_ADC_ConfigChannel(&hadc, &sConfig);


	sConfig.Channel = ADC_CHANNEL_5;
	sConfig.Rank = ADC_RANK_CHANNEL_NUMBER;
	HAL_ADC_ConfigChannel(&hadc, &sConfig);

	sConfig.Channel = ADC_CHANNEL_VREFINT;
	HAL_ADC_ConfigChannel(&hadc, &sConfig);
}



uint16_t ADC_Avg_Value(uint32_t channel)
{
	uint32_t max= 0x00000000;
	uint32_t min= 0xFFFFFFFF;
	uint32_t sum=0;
	uint32_t curr_val;//current value
	uint8_t i;

	/* Select Channel  to be converted*/
	hadc.Instance->CHSELR = (uint32_t)(channel & ADC_CHANNEL_MASK);


	for(i=0;i<10;i++)
	{
		HAL_ADC_Start_IT(&hadc);

		while(ADC_IT_flag == 0 ); //waiting for end of conversion

		curr_val = HAL_ADC_GetValue(&hadc);
		sum += curr_val;
		max = (curr_val>max) ? curr_val : max;
		min = (curr_val<min) ? curr_val : min;

		ADC_IT_flag=0;

	}
		sum -= (max+min);
		curr_val= (uint16_t) ( sum >> 3 ); //division by 8

		return curr_val;
}



uint32_t ADC_Get_Voltage (uint32_t channel)
{
	uint16_t ADC_data;
	uint64_t ADC_voltage;


	ADC_data= ADC_Avg_Value(channel);

	ADC_voltage = ( (uint64_t) 3000* (uint64_t) vrefint_cal* (uint64_t) ADC_data) / ( (uint64_t) vrefint_data* (uint64_t) FULL_SCALE);

	return (uint32_t) ADC_voltage;// mV
}



uint32_t Get_Battery_Voltage(uint32_t channel)
{
	uint32_t battery_voltage;

	battery_voltage = ADC_Get_Voltage(channel);
	battery_voltage += (battery_voltage/9) ;//compensate 10k RC filter series resistance

	return battery_voltage;
}



void Get_Battery_Level(uint32_t channel){

	uint32_t voltage;

	voltage = Get_Battery_Voltage(channel);

	if(voltage >= BATTERY_VOLTAGE_MAX){
		battery_level=100;
	}

	else{
		if(voltage <= BATTERY_VOLTAGE_MIN){
			battery_level=0;
		}

		else{
			voltage = (voltage - BATTERY_VOLTAGE_MIN) >> 2; //battery_level = (voltage-BATTERY_VOLTAGE_MIN) / 4
			battery_level = (uint8_t) voltage;
		}
	}
}


uint32_t Get_Pressure(uint32_t channel)
{
	uint32_t v_out;
	int64_t p;

	v_out = ADC_Get_Voltage(channel);

	/*
	//MPXH6300
	p = ( v_out * 129 + 1137 );
	p = (p >> 10);
	*/


	/*MPXH6400A*/
	p = ( v_out * 169 + 3561 );
	p = (p >> 10);



	return (uint32_t) p;
}



void Remove_Pressure_Offset(void){
	//remove DC offset
		pressure =  pressure - pressure_DC_offset;

}


void Write_Pressure_Offset_To_EEPROM(void){
	HAL_FLASHEx_DATAEEPROM_Unlock();
	HAL_FLASHEx_DATAEEPROM_Program(FLASH_TYPEPROGRAMDATA_WORD, (uint32_t)PRESSURE_OFFSET_ADDR, pressure_DC_offset);
	HAL_FLASHEx_DATAEEPROM_Lock();
}

void Read__Pressure_Offset_From_EEPROM(void){

	pressure_DC_offset = *((int32_t*)PRESSURE_OFFSET_ADDR);

	if(pressure_DC_offset > 50 | pressure_DC_offset < -50){
		pressure_DC_offset = 0;
	}
}




void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* AdcHandle)
{
	ADC_IT_flag++;
}
