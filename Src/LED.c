/*
 * LED.c
 *
 *  Created on: 2016. �pr. 7.
 *      Author: Andras
 */

#include "LED.h"


TIM_HandleTypeDef htim2;
uint8_t color[]= {0, 1, 0};



/* TIM2 init function */
void MX_TIM2_Init(void)
{

  //TIM_MasterConfigTypeDef sMasterConfig;
  TIM_OC_InitTypeDef sConfigOC;
  //Prescaler = (TIM2CLK / TIM2 counter clock) - 1; (2MHz/25000Hz)-1=79
  //ARR = (TIM2 counter clock / TIM2 output clock) - 1; ARR=period, (25000Hz/100)-1=249
  htim2.Instance = TIM2;
  htim2.Init.Prescaler = 79;
  htim2.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim2.Init.Period = 249;
  htim2.Init.ClockDivision = TIM_CLOCKDIVISION_DIV4; //8MHz/4=2MHz
  HAL_TIM_PWM_Init(&htim2);


  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;


  //TIM2 Channel1 duty cycle = (TIM2_CCR1/ TIM2_ARR + 1)* 100

  sConfigOC.Pulse = 1;
  HAL_TIM_PWM_ConfigChannel(&htim2, &sConfigOC, TIM_CHANNEL_1);

  sConfigOC.Pulse = 0;
  HAL_TIM_PWM_ConfigChannel(&htim2, &sConfigOC, TIM_CHANNEL_2);

  sConfigOC.Pulse = 0;
  HAL_TIM_PWM_ConfigChannel(&htim2, &sConfigOC, TIM_CHANNEL_3);



  //start PWM
  HAL_TIM_PWM_Start(&htim2, TIM_CHANNEL_1);
  HAL_TIM_PWM_Start(&htim2, TIM_CHANNEL_2);
  HAL_TIM_PWM_Start(&htim2, TIM_CHANNEL_3);
}




void Set_LED_Color(uint8_t *color)
{
//color[0] : Red value (  0-249 )
//color[1] : Green value (  0-249 )
//color[2] : Blue value (  0-249 )

	uint8_t i;

	for(i=0; i<3; i++)
	{
		if(color[i] > 249)
		{
			color[i]=249;
		}
	}


	__HAL_TIM_SetCompare(&htim2, TIM_CHANNEL_1, color[0]);
	__HAL_TIM_SetCompare(&htim2, TIM_CHANNEL_2, color[1]);
	__HAL_TIM_SetCompare(&htim2, TIM_CHANNEL_3, color[2]);

}
