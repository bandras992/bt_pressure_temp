/**
  ******************************************************************************
  * File Name          : main.c
  * Description        : Main program body
  ******************************************************************************
  *
  * COPYRIGHT(c) 2015 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/


#include "bluetooth.h"
#include "stm32l0xx_hal.h"

/* Private variables ---------------------------------------------------------*/


TIM_HandleTypeDef htim6;

volatile uint8_t tim6_IT_flag=0;//TIM6 interrupt flag
volatile uint8_t tim6_IT_state=0;

circular_buffer c_buffer;
uint8_t BT_ready_flag=0;
uint8_t BT_init_state=0;


/* Private function prototypes -----------------------------------------------*/
static void SystemClock_Config(void);
static void TIM6_Init(void);



int main(void)
{

  /* MCU Configuration----------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
   HAL_Init();
   MX_GPIO_Init();

  /* Configure the system clock */
   SystemClock_Config();

  /* Initialize all configured peripherals */
   MX_TIM2_Init();
   MX_I2C1_Init();
   Temp_Sensor_Init();
   MX_ADC_Init();
   vrefint_data = ADC_Avg_Value(ADC_CHANNEL_VREFINT);

   Read__Pressure_Offset_From_EEPROM();
   //pressure_DC_offset = Get_Pressure_DC_Offset(ADC_CHANNEL_5);


   MX_USART1_UART_Init();

   /* Enable the UART Data Register not empty Interrupt */
   RXNE_IT_Enable(&huart1);

   /*
   BT_Init();

   Set_LED_Color(color);
   Write_LED_Color_To_Characteristic();
	*/

   TIM6_Init();

  while (1)
  {
	  /* List sercices
	   *
	   * HAL_UART_Transmit(&huart1,&BT_command_array[4].command[0] ,BT_command_array[4].size ,1000 );// "LS"
	  HAL_UART_Transmit(&huart1,&BT_command_array[3].command[0] ,BT_command_array[3].size ,1000 );// "/r/n"

	  HAL_Delay(2000);*/

	  UART_State_Machine(&c_buffer);

	  switch(BT_ready_flag){


	  case 0:

		  BT_Init_SM();

	  break;



	  case 1:

	  	  	  if (tim6_IT_flag){// 5Hz

	  	  		  pressure = Get_Pressure(ADC_CHANNEL_5);
	  	  		  Remove_Pressure_Offset();
	  	  		  Write_Pressure_To_Characteristic();
	  	  		  Temp_Sensor_Read();
	  	  		  Write_Temp_To_Characteristic();

	  	  		  tim6_IT_flag=0;
	  	  		  tim6_IT_state = (tim6_IT_state > 10) ? 0 : tim6_IT_state ;

	  	  	  }

	  	  	  if(10 == tim6_IT_state){ // 0.5Hz
	  	  		  vrefint_data = ADC_Avg_Value(ADC_CHANNEL_VREFINT); //recalibrate (temperature-depent!) internal reference
	  	  		  Get_Battery_Level(ADC_CHANNEL_3);
	  	  		  Write_Battery_Level_To_Characteristic();
	  	  		  Detect_Remote_Temp_Sensor();

	  	  		  if(battery_level == 0){
	  	  			  Turn_Off();
	  	  		  }


	  	  		  tim6_IT_state=0;
	  	  	  }


	  break;

	  default:

		  BT_ready_flag = 0;
		  BT_init_state = 0;

	  break;




	  }

  }
}


/** System Clock Configuration
*/
void SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_PeriphCLKInitTypeDef PeriphClkInit;


  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI; //HIGH SPEDD INTERNAL OSC
  RCC_OscInitStruct.HSIState = RCC_HSI_DIV4;
  RCC_OscInitStruct.HSICalibrationValue = 16;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLLMUL_4;
  RCC_OscInitStruct.PLL.PLLDIV = RCC_PLLDIV_2;
  HAL_RCC_OscConfig(&RCC_OscInitStruct);

  RCC_ClkInitStruct.ClockType = (RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2);
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;
  HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0);


  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USART1|RCC_PERIPHCLK_I2C1;
  PeriphClkInit.Usart1ClockSelection = RCC_USART1CLKSOURCE_PCLK2;
  PeriphClkInit.I2c1ClockSelection = RCC_I2C1CLKSOURCE_PCLK1;
  HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit);


  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);


  __PWR_CLK_ENABLE();

  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE2);

  __HAL_RCC_PWR_CLK_DISABLE();


}




void TIM6_Init(void)
{
	TIM_OC_InitTypeDef sConfigOC;

	//Prescaler = (TIM6CLK / TIM6 counter clock) - 1; (8MHz/4kHz)-1=1999
	//ARR = (TIM6 counter clock / TIM6 output clock) - 1; ARR=period, (4kHz/5Hz)-1=799
	htim6.Instance = TIM6;
	htim6.Init.Prescaler = 1999;
	htim6.Init.CounterMode = TIM_COUNTERMODE_UP;
	htim6.Init.Period = 799;
	htim6.Init.ClockDivision = TIM_CLOCKDIVISION_DIV4; //8MHz/4=2MHz
	HAL_TIM_Base_Init(&htim6);

	HAL_TIM_Base_Start_IT(&htim6); //Start the TIM Base generation in interrupt mode

}


//TIM6 interrupt handler
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim){
	if(htim==&htim6)
	{
		//h�m�rs�klet beolvas�s, k�ld�s, minden 100.n�l vbatt beolvas�s, ellen�rz�s
		tim6_IT_flag++;
		tim6_IT_state++;

	}

}


#ifdef USE_FULL_ASSERT

/**
   * @brief Reports the name of the source file and the source line number
   * where the assert_param error has occurred.
   * @param file: pointer to the source file name
   * @param line: assert_param error line source number
   * @retval None
   */
void assert_failed(uint8_t* file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
    ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */

}

#endif

/**
  * @}
  */ 

/**
  * @}
*/ 

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
